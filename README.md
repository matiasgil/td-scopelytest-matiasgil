# TD-ScopelyTest-MatiasGil


## Brief

This is a technical evaluation by Matias Gil for Scopely Company.

-MVP (Architectural Design)

-Reactive (Using UniRx library)

-Dependency Inyection (Own solution)

-Assembly Definitions (To ensure the modules independency for high cohesion and low coupling)

-SOLID Principles

-TDD (I had the intention to work with test driven development, but since I notice the scale of the project I couldn't accomplish that, instead I added 1 test in order to show that i can do TDD).



## Logbook

22-02: Architectural design, I was trying to decide between ECS with MVVM or Model-View-Presenter, I ended up going for MVP.

23-02: Implemented Booter, Home and Simple Map modules (I started working with TDD on the map module but I figured out it was going to take forever if you used TDD on the whole technical test, so there is only 1 test).

24-02: Added Spawn Points Module, the configuration of the spawn points is obtained from a reactive repository, this should be easily be replaced for a repository that fetches the config from a backend (Ideally a rest api).

24-02: Added Game Clock module (This should be the heartbeat of the game during gameplay).

24-02: Started with enemy module, adding the structure of the module.

25-02: Completed enemy spawners, same as Spawn Points Module the configuration for this module is obtained from a reactive repository, enabling changing where the config is obtained by simply changing the repository implementation. This makes the project way more scalable and not thinking on technologies during development.

25-02: Changed the camera view and added a bit of fun to the map.

25-02: Enemies added and synced with the Map Module. By this point I figure out that I could have done the test faster by implementing ECM since it's faster to develop a Minimum viable product that way.

25-02: I ended up adding towers and game cycle without following the architecture since I can't afford to spend more time on the project and i wanted to make it functional. (Most of the code added on this point is trash and non efficient, I'm a bit ashemed of it, lol.)


## Roadmap - What could be the readmap if the project continues.

-Implement Towers following the Architectural design (On the view implementation of the towers there is some hint on how to do this on comments).

-Add Economy system (since right now the game is too easy to win, since there is no limits on towers)

-Add a dynamic navmesh updated every time a tower is placed, right now you can circle the spawn point with towers and enemies cannot scape.

-Limit the towers positions so they cannot be placed on top of each other.

-Add more levels, and complete the map module to spawn maps based on a Map ID.

-Implement a Rest Gateway Module on client and Rest Api where all configuration can be obtained from.
