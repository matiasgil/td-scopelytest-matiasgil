﻿namespace Utils
{
    public struct PlanePosition
    {
        public readonly int X;
        public readonly int Y;

        public PlanePosition(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}