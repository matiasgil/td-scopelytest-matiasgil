﻿using System;
using System.Collections.Generic;

namespace Utils
{
    public static class DependencyProvider
    {
        static readonly Dictionary<string, object> Instances = new Dictionary<string, object>();

        static DependencyProvider()
        {
        }

        public static T Get<T>(string id = null)
        {
            var type = typeof(T).FullName;
            if (id != null)
                type = id;
            object singletonObj;

            if (Instances.TryGetValue(type, out singletonObj))
                return (T) singletonObj;

            return default(T);
        }

        public static void Set<T>(object singletonObj, string id = null)
        {
            var type = typeof(T).FullName;
            if (id != null)
                type = id;
            Instances[type] = singletonObj;
        }

        public static T GetOrInstanciate<T>(Func<object> instantiator, string id = null)
        {
            var type = typeof(T).FullName;
            if (id != null)
                type = id;

            object singletonObj = Get<T>(type);
            object nullValue = default(T);

            if (singletonObj != nullValue)
                return (T) singletonObj;
            return Instanciate<T>(instantiator, type);
        }

        public static T Instanciate<T>(Func<object> instanciator, string id)
        {
            var singletonObj = instanciator();

            Instances[id] = singletonObj;
            return (T) singletonObj;
        }
    }
}