﻿using System;
using TowerDefense.Home.Adapter;

namespace TowerDefense.Home.Core.Actions
{
    public class SelectMapAction
    {
        private readonly IHomeMapAdapter _homeMapAdapter;

        public SelectMapAction(IHomeMapAdapter homeMapAdapter)
        {
            _homeMapAdapter = homeMapAdapter;
        }

        public IObservable<string> Execute(string mapId)
        {
            return _homeMapAdapter.SelectMap(mapId);
        }
    }
}