﻿using TowerDefense.Home.Adapter;
using TowerDefense.Home.Core.Actions;
using Utils;

namespace TowerDefense.Home.Infrastructure.Providers
{
    public static class HomeActionProvider
    {
        public static SelectMapAction ProvideSelectMapAction()
        {
            return new SelectMapAction(DependencyProvider.Get<IHomeMapAdapter>());
        }
    }
}