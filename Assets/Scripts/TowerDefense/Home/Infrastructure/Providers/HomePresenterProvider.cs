﻿using TowerDefense.Home.Presentation;

namespace TowerDefense.Home.Infrastructure.Providers
{
    public static class HomePresenterProvider
    {
        public static HomePresenter ProvideFor(IHomeView homeView)
        {
            return new HomePresenter(homeView,
                HomeActionProvider.ProvideSelectMapAction());
        }
    }
}