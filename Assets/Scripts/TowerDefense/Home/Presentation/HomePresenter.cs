﻿using System;
using TowerDefense.Home.Core.Actions;
using UniRx;

namespace TowerDefense.Home.Presentation
{
    public class HomePresenter
    {
        private readonly IHomeView _homeView;
        private readonly SelectMapAction _selectMapAction;

        public HomePresenter(IHomeView homeView, SelectMapAction selectMapAction)
        {
            _homeView = homeView;
            _selectMapAction = selectMapAction;

            SetUpView();
        }

        private void SetUpView()
        {
            var disposable = _homeView.OnStartButtonClick()
                .SelectMany(SelectMap("map_1"))
                .SelectMany(_homeView.NavigateToMapScene())
                .Subscribe();
            _homeView.DisposeWhenDestroyed(disposable);
        }

        private IObservable<string> SelectMap(string mapId)
        {
            return _selectMapAction.Execute(mapId);
        }
    }
}