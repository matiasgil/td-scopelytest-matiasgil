﻿using System;
using UniRx;

namespace TowerDefense.Home.Presentation
{
    public interface IHomeView
    {
        public IObservable<Unit> OnStartButtonClick();
        public IObservable<Unit> NavigateToMapScene();
        void DisposeWhenDestroyed(IDisposable disposable);
    }
}