﻿using TowerDefense.Home.Adapter;
using TowerDefense.Home.Infrastructure.Providers;
using Utils;

namespace TowerDefense.Home
{
    public class HomeModule
    {
        private HomeModule(IHomeMapAdapter homeMapAdapter)
        {
            DependencyProvider.Set<IHomeMapAdapter>(homeMapAdapter);
        }

        public static HomeModule Init(IHomeMapAdapter homeMapAdapter)
        {
            return new HomeModule(homeMapAdapter);
        }
    }
}