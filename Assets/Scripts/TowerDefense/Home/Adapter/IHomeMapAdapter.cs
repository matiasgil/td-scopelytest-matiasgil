﻿using System;

namespace TowerDefense.Home.Adapter
{
    public interface IHomeMapAdapter
    {
        public IObservable<string> SelectMap(string mapId);
    }
}