﻿using System;
using TowerDefense.Home.Infrastructure.Providers;
using TowerDefense.Home.Presentation;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils.Maybe;

namespace TowerDefense.Home.UnityDelivery
{
    public class UnityHomeView : MonoBehaviour, IHomeView
    {
        [SerializeField] private Button startButton;
        private Maybe<HomePresenter> _homePresenter = Maybe<HomePresenter>.Nothing;

        private void Awake()
        {
            _homePresenter = HomePresenterProvider.ProvideFor(this).ToMaybe();
        }

        public IObservable<Unit> OnStartButtonClick()
        {
            return startButton.onClick.AsObservable();
        }

        public IObservable<Unit> NavigateToMapScene()
        {
            return Observable.ReturnUnit().Do(x =>{ SceneManager.LoadScene("MapScene");});
        }

        public void DisposeWhenDestroyed(IDisposable disposable)
        {
            disposable.AddTo(gameObject);
        }

        /*
        public IObservable<Unit> NavigateToMapScene()
        {
            
            return Observable.Create<Unit>(subject =>
            {
                subject.OnNext(SceneManager.LoadScene("MapScene"));
                subject.OnCompleted();
                return Disposable.Empty;
            });
        }
        */
    }
}