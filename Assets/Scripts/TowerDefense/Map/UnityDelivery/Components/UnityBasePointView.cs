using System;
using TowerDefense.Map.Infrastructure.Providers;
using TowerDefense.Map.Presentation;
using UniRx;
using UnityEngine;

namespace TowerDefense.Map.UnityDelivery.Components
{
    public class UnityBasePointView : MonoBehaviour, IBasePointView
    {
        private readonly Subject<Unit> _enemyEnteredBase = new Subject<Unit>();

        private void Awake()
        {
            MapPresenterProvider.ProvideFor(this);
        }

        public IObservable<Unit> EnemyEnteredBase()
        {
            return _enemyEnteredBase;
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Enemy"))
            {
                _enemyEnteredBase.OnNext(Unit.Default);
                Destroy(other.gameObject);
            }       
        }
        
        public void DisposeWhenDestroyed(IDisposable disposable)
        {
            disposable.AddTo(gameObject);
        }
    }
}