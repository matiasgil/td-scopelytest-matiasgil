using UnityEngine;

namespace TowerDefense.Map.UnityDelivery.Components
{
    public class AngledCameraMovement : MonoBehaviour
    {
        [SerializeField]
        private Camera _thisCamera;

        [SerializeField] private Rect cameraBoundries;
        
        private const int PIXEL_THRESHOLD = 4;
        private const float CAMERA_SPEED = 25f;

        private void Start()
        {
            _thisCamera = GetComponent<Camera>();
        }

        void Update()
        {
            var cameraPosition = _thisCamera.transform.position;

            var horizontalMouseDirection = GetAxisMouseDirection(Input.mousePosition.x, Screen.width);
            var verticalMouseDirection = GetAxisMouseDirection(Input.mousePosition.y, Screen.height);
            var targetCameraPosition = Vector3.Lerp(cameraPosition,
                cameraPosition + new Vector3(horizontalMouseDirection, 0, verticalMouseDirection),
                CAMERA_SPEED * Time.deltaTime);
        
            if (horizontalMouseDirection != 0 || verticalMouseDirection != 0)
            {
                if (horizontalMouseDirection != 0 && 
                    !cameraBoundries.Contains(new Vector2(targetCameraPosition.x, 0)))
                {
                    targetCameraPosition = new Vector3(cameraPosition.x, cameraPosition.y, targetCameraPosition.z);
                }

                if (verticalMouseDirection != 0 &&
                    !cameraBoundries.Contains(new Vector2(0, targetCameraPosition.z)))
                {
                    targetCameraPosition = new Vector3(targetCameraPosition.x, cameraPosition.y, cameraPosition.z);
                }
            
                _thisCamera.transform.position = targetCameraPosition;
            }
        }

        private int GetAxisMouseDirection(float mouseAxisPos, float screenAxisSize)
        {
            if (mouseAxisPos < PIXEL_THRESHOLD)
            {
                return -1;
            }

            return mouseAxisPos > screenAxisSize - PIXEL_THRESHOLD ? 1 : 0;
        }
    }
}
