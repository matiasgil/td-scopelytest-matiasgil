﻿using System;
using TowerDefense.Map.Infrastructure.Providers;
using TowerDefense.Map.Presentation;
using UnityEngine;
using Utils;

namespace TowerDefense.Map.UnityDelivery
{
    public class UnityMapView : MonoBehaviour, IMapView
    {
        public event Action Init = () => { };
        
        private const int PLANE_SCALE_RELATION = 5;

        [SerializeField] private GameObject mapFloor;

        [SerializeField] private GameObject genericSpawnPointPrefab;

        [SerializeField] private GameObject basePrefab;

        [SerializeField] private Transform spawnPointsParent;

        public void Awake()
        {
            MapPresenterProvider.ProvideFor(this);
        }

        public void Start()
        {
            Init.Invoke();
        }

        public void ShowMap(string mapId)
        {
            //TODO: show specific map based on ID
        }

        public void ShowSpawnPoint(string spawnPointId, PlanePosition position)
        {
            var spawnPoint = Instantiate(genericSpawnPointPrefab, spawnPointsParent.transform, true);
            spawnPoint.transform.localPosition = new Vector3(position.X, 0, position.Y);
        }

        public void ShowBase(PlanePosition basePosition)
        {
            var spawnPoint = Instantiate(basePrefab, spawnPointsParent.transform, true);
            spawnPoint.transform.localPosition = new Vector3(basePosition.X, 0, basePosition.Y);
        }
    }
}