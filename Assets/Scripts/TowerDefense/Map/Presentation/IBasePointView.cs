﻿using System;
using UniRx;

namespace TowerDefense.Map.Presentation
{
    public interface IBasePointView
    {
        IObservable<Unit> EnemyEnteredBase();
        void DisposeWhenDestroyed(IDisposable disposable);
    }
}