﻿using TowerDefense.Map.Core.Actions;
using TowerDefense.Map.Core.Domain;
using UniRx;

namespace TowerDefense.Map.Presentation
{
    public class MapPresenter
    {
        private readonly IMapView _view;
        private readonly CreateMapAction _createMap;

        public MapPresenter(IMapView view,
            CreateMapAction createMap)
        {
            _createMap = createMap;
            
            _view = view;
            _view.Init += SetUp;
        }

        private void SetUp()
        {
            _createMap.Execute()
                .Do(ShowSpawnPoints)
                .Do(gameMap => _view.ShowBase(gameMap.BasePosition()))
                .Subscribe(ShowMapOnView)
                .Dispose();
        }

        private void ShowSpawnPoints(GameMap gameMap)
        {
            foreach (var spawnPoint in gameMap.SpawnPoints())
            {
                _view.ShowSpawnPoint(spawnPoint.Id, spawnPoint.Position());
            }
        }

        private void ShowMapOnView(GameMap gameMap)
        {
            _view.ShowMap(gameMap.MapId());
        }
    }
}