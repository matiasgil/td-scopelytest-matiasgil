﻿using TowerDefense.Map.Adapter;
using UniRx;

namespace TowerDefense.Map.Presentation
{
    public class BasePresenter
    {
        private readonly IBasePointView _view;
        private readonly IMapGameLevelAdapter _mapGameLevelAdapter;

        public BasePresenter(IBasePointView view,
            IMapGameLevelAdapter mapGameLevelAdapter)
        {

            mapGameLevelAdapter.NewGame();
            
            _view = view;
            _mapGameLevelAdapter = mapGameLevelAdapter;

            var disposable = _view.EnemyEnteredBase()
                .Do(x => mapGameLevelAdapter.EnemyEnteredTheBase())
                .Subscribe();

            _view.DisposeWhenDestroyed(disposable);
        }
    }
}