﻿using System;
using UnityEngine;
using Utils;

namespace TowerDefense.Map.Presentation
{
    public interface IMapView
    {
        event Action Init;
        void ShowMap(string mapId);
        void ShowSpawnPoint(string spawnPointId, PlanePosition position);
        void ShowBase(PlanePosition basePosition);
    }
}