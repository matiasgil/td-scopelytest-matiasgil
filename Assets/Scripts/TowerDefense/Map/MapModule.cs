﻿using System;
using System.Linq;
using TowerDefense.Map.Adapter;
using TowerDefense.Map.Infrastructure.Providers;
using TowerDefense.Map.Infrastructure.Repository;
using UniRx;
using Utils;

namespace TowerDefense.Map
{
    public class MapModule
    {
        private readonly ISelectedMapIdRepository _selectedMapRepository;
        private readonly IMapRepository _mapRepository;

        private MapModule(IMapGameLevelAdapter mapGameLevelAdapter)
        {
            _selectedMapRepository = MapRepositoryProvider.ProvideSelectedMapIdRepository();
            _mapRepository = MapRepositoryProvider.ProvideMapRepository();
            DependencyProvider.Set<IMapGameLevelAdapter>(mapGameLevelAdapter);
        }

        public static MapModule Init(IMapGameLevelAdapter mapGameLevelAdapter)
        {
            return new MapModule(mapGameLevelAdapter);
        }

        public IObservable<string> SetMapId(string mapId)
        {
            return _selectedMapRepository.SaveSelectedMapId(mapId);
        }

        public IObservable<string> GetSelectedMap()
        {
            return _selectedMapRepository.GetSelectedMapId();
        }

        public IObservable<PlanePosition> GetMapSpawnPoint()
        {
            return _mapRepository.GetCurrentMap()
                .Select(x => x.SpawnPoints().First().Position());
        }

        public IObservable<PlanePosition> GetMapBasePoint()
        {
            return _mapRepository.GetCurrentMap()
                .Select(x => x.BasePosition());
        }
    }
}