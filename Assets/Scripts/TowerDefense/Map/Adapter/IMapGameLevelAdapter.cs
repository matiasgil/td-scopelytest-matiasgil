﻿namespace TowerDefense.Map.Adapter
{
    public interface IMapGameLevelAdapter
    {
        void EnemyEnteredTheBase();
        void NewGame();
    }
}