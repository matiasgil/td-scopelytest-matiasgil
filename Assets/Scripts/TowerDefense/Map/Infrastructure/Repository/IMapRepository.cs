﻿using System;
using TowerDefense.Map.Core.Domain;

namespace TowerDefense.Map.Infrastructure.Repository
{
    public interface IMapRepository
    {
        public IObservable<GameMap> SaveCurrentMap(GameMap currentGameMap);
        public IObservable<GameMap> GetCurrentMap();
    }
}