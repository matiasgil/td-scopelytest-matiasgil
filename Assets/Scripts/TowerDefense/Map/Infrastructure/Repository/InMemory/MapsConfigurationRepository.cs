﻿using System;
using System.Collections.Generic;
using TowerDefense.Map.Core.Domain.DTO;
using TowerDefense.Map.Core.Domain.Exceptions;
using UniRx;
using Utils;
using Utils.Maybe;

namespace TowerDefense.Map.Infrastructure.Repository.InMemory
{
    public class MapsConfigurationRepository : IMapsConfigurationRepository
    {
        private readonly Dictionary<string, Maybe<MapConfigurationDto>> _mapConfigurations;

        /// <summary>
        /// Since this is a technical test map configuration is hardcoded on this Repo implementation,
        /// but it could be replaced for a repository that fetches data from a backend.
        /// </summary>
        public MapsConfigurationRepository()
        {
            _mapConfigurations = new Dictionary<string, Maybe<MapConfigurationDto>>();
            var mockedMapConfig = GetMockMapConfig();
            _mapConfigurations.Add(mockedMapConfig.Id, mockedMapConfig.ToMaybe());
        }

        public IObservable<MapConfigurationDto> GetMapConfigFor(string mapId)
        {
            
            return Observable.Create<MapConfigurationDto>(observer =>
            {
                 _mapConfigurations[mapId]
                    .Do(observer.OnNext)
                    .DoWhenAbsent(() => observer.OnError(new InvalidMapConfigurationException("Invalid MapId")));
                 
                 observer.OnCompleted();
                 
                 return Disposable.Empty;
            });
        }

        private MapConfigurationDto GetMockMapConfig()
        {
            var spawnPoints = new Dictionary<string, PlanePosition>();
            spawnPoints.Add("spawner_01", new PlanePosition(-19, 14));
            return new MapConfigurationDto("map_1", 20, 15, spawnPoints, new PlanePosition(18 ,-13));
        }
    }
}