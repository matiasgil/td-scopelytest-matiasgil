﻿using System;
using TowerDefense.Map.Core.Domain;
using UniRx;
using Utils.Maybe;

namespace TowerDefense.Map.Infrastructure.Repository.InMemory
{
    public class MapRepository : IMapRepository
    {
        private Maybe<GameMap> _currentMap = Maybe<GameMap>.Nothing;
        public IObservable<GameMap> SaveCurrentMap(GameMap currentGameMap)
        {
            return Observable.Create<GameMap>(observer =>
                {
                    _currentMap = currentGameMap.ToMaybe();
                    
                    //_currentMap.Do(observer.OnNext)
                    //.DoWhenAbsent();  Throw specific exception
                    _currentMap.Do(observer.OnNext);
                    
                    observer.OnCompleted();
                    
                    return Disposable.Empty;      
                }
            );
        }

        public IObservable<GameMap> GetCurrentMap()
        {
            return Observable.Create<GameMap>(observer =>
                {
                    _currentMap.Do(observer.OnNext);
                    
                    observer.OnCompleted();
                    
                    return Disposable.Empty;      
                }
            );
        }
    }
}