﻿using System;
using UniRx;
using Utils.Maybe;

namespace TowerDefense.Map.Infrastructure.Repository.InMemory
{
    public class SelectedMapIdRepository : ISelectedMapIdRepository
    {
        private Maybe<string> _selectedMapId = Maybe<string>.Nothing;
        
        public IObservable<string> GetSelectedMapId()
        {
            return Observable.Create<string>(observer =>
                {
                    _selectedMapId.Do(observer.OnNext);
                    
                    observer.OnCompleted();
                    
                    return Disposable.Empty;      
                }
            );
        }

        public IObservable<string> SaveSelectedMapId(string mapId)
        {
            return Observable.Create<string>(observer =>
                {
                    _selectedMapId = mapId.ToMaybe();
                    
                    observer.OnNext(mapId);
                    
                    observer.OnCompleted();
                    
                    return Disposable.Empty;      
                }
            );
        }
    }
}