﻿using System;

namespace TowerDefense.Map.Infrastructure.Repository
{
    public interface ISelectedMapIdRepository
    {
        public IObservable<string> GetSelectedMapId();
        public IObservable<string> SaveSelectedMapId(string mapId);
    }
}