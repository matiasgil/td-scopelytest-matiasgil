﻿using System;
using TowerDefense.Map.Core.Domain.DTO;
using UniRx;

namespace TowerDefense.Map.Infrastructure.Repository
{
    public interface IMapsConfigurationRepository
    {
        public IObservable<MapConfigurationDto> GetMapConfigFor(string mapId);
    }
}