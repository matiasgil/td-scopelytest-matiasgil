﻿using TowerDefense.Map.Adapter;
using TowerDefense.Map.Presentation;
using TowerDefense.Map.UnityDelivery.Components;
using UniRx;
using Utils;

namespace TowerDefense.Map.Infrastructure.Providers
{
    public static class MapPresenterProvider
    {
        public static MapPresenter ProvideFor(IMapView view)
        {
            return new MapPresenter(view,
                MapActionProvider.ProvideCreateMapAction());
        }

        public static BasePresenter ProvideFor(IBasePointView view)
        {
            return new BasePresenter(view,
                DependencyProvider.Get<IMapGameLevelAdapter>());
        }
    }
}