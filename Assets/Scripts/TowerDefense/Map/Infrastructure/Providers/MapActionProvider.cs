﻿using TowerDefense.Map.Core.Actions;

namespace TowerDefense.Map.Infrastructure.Providers
{
    public static class MapActionProvider
    {
        public static CreateMapAction ProvideCreateMapAction()
        {
            return new CreateMapAction(MapRepositoryProvider.ProvideMapRepository(),
                MapRepositoryProvider.ProvideMapsConfigurationRepository(),
                MapRepositoryProvider.ProvideSelectedMapIdRepository());
        }
    }
}