﻿using TowerDefense.Map.Core.Actions;
using TowerDefense.Map.Infrastructure.Repository;
using TowerDefense.Map.Infrastructure.Repository.InMemory;
using Utils.Maybe;

namespace TowerDefense.Map.Infrastructure.Providers
{
    public static class MapRepositoryProvider
    {
        private static Maybe<MapRepository> _mapRepository = Maybe<MapRepository>.Nothing;   
        public static IMapRepository ProvideMapRepository()
        {
            _mapRepository.DoWhenAbsent(() => _mapRepository = new MapRepository().ToMaybe());
            return _mapRepository.Value;
        }

        private static Maybe<MapsConfigurationRepository> _mapsConfigurationRepository = Maybe<MapsConfigurationRepository>.Nothing;
        public static IMapsConfigurationRepository ProvideMapsConfigurationRepository()
        {
            _mapsConfigurationRepository.DoWhenAbsent(() => _mapsConfigurationRepository = new MapsConfigurationRepository().ToMaybe());
            return _mapsConfigurationRepository.Value;
        }

        private static Maybe<SelectedMapIdRepository> _selectedMapIdRepository = Maybe<SelectedMapIdRepository>.Nothing;
        public static ISelectedMapIdRepository ProvideSelectedMapIdRepository()
        {
            _selectedMapIdRepository.DoWhenAbsent(() => _selectedMapIdRepository = new SelectedMapIdRepository().ToMaybe());
            return _selectedMapIdRepository.Value;
        }
    }
}