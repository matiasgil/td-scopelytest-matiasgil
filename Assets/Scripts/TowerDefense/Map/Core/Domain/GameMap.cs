﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Utils;

namespace TowerDefense.Map.Core.Domain
{
    public class GameMap
    {
        private readonly string _mapId;
        private int _width;
        private int _height;
        
        private readonly ReadOnlyCollection<SpawnPoint> _spawnPoints;

        private readonly PlanePosition _basePosition;

        public GameMap(string mapId, 
            int width, 
            int height, 
            Dictionary<string, PlanePosition> spawnPoints,
            PlanePosition basePosition)
        {
            _mapId = mapId;
            _width = width;
            _height = height;

            _basePosition = basePosition;
            _spawnPoints = BuildSpawnPointsFrom(spawnPoints);
        }

        public string MapId()
        {
            return _mapId;
        }

        public int Width()
        {
            return _width;
        }

        public int Height()
        {
            return _height;
        }

        public PlanePosition BasePosition()
        {
            return _basePosition;
        }

        public ReadOnlyCollection<SpawnPoint> SpawnPoints()
        {
            return _spawnPoints;
        }
        
        private ReadOnlyCollection<SpawnPoint> BuildSpawnPointsFrom(Dictionary<string, PlanePosition> spawnPoints)
        {
            var tempSpawnPointList = spawnPoints.Select(spawnPoint => new SpawnPoint(spawnPoint.Key, spawnPoint.Value)).ToList();

            return tempSpawnPointList.AsReadOnly();
        }
    }
}