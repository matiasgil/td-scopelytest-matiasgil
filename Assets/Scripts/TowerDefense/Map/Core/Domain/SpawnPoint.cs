﻿using Utils;

namespace TowerDefense.Map.Core.Domain
{
    public class SpawnPoint
    {
        public readonly string Id;
        private PlanePosition _position;

        public SpawnPoint(string id, PlanePosition position)
        {
            Id = id;
            _position = position;
        }

        public PlanePosition Position()
        {
            return _position;
        }
    }
}