﻿using System.Collections.Generic;
using Utils;

namespace TowerDefense.Map.Core.Domain.DTO
{
    public readonly struct MapConfigurationDto
    {
        public readonly string Id;
        public readonly int Width;
        public readonly int Height;
        public readonly Dictionary<string, PlanePosition> SpawnPoints;
        public readonly PlanePosition BasePosition;

        public MapConfigurationDto(string id,
            int width, 
            int height,
            Dictionary<string, PlanePosition> spawnPoints,
            PlanePosition basePosition)
        {
            Id = id;
            Width = width;
            Height = height;
            SpawnPoints = spawnPoints;
            BasePosition = basePosition;
        }
    }
}