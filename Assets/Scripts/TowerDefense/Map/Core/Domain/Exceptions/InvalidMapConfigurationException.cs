﻿using System;

namespace TowerDefense.Map.Core.Domain.Exceptions
{
    [Serializable]
    public class InvalidMapConfigurationException : Exception
    {
        public InvalidMapConfigurationException() { }

        public InvalidMapConfigurationException(string message)
            : base(message) { }

        public InvalidMapConfigurationException(string message, Exception inner)
            : base(message, inner) { }
    }
}