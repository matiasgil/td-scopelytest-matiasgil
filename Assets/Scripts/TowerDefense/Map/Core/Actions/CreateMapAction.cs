using System;
using TowerDefense.Map.Core.Domain;
using TowerDefense.Map.Core.Domain.DTO;
using TowerDefense.Map.Infrastructure.Repository;
using UniRx;

namespace TowerDefense.Map.Core.Actions
{
    public class CreateMapAction
    {
        private readonly IMapRepository _mapRepository;
        private readonly IMapsConfigurationRepository _mapsConfigurationRepository;
        private readonly ISelectedMapIdRepository _selectedMapIdRepository;

        public CreateMapAction(){}

        public CreateMapAction(IMapRepository mapRepository,
            IMapsConfigurationRepository mapsConfigurationRepository,
            ISelectedMapIdRepository selectedMapIdRepository)
        {
            _mapRepository = mapRepository;
            _mapsConfigurationRepository = mapsConfigurationRepository;
            _selectedMapIdRepository = selectedMapIdRepository;
        }

        public virtual IObservable<GameMap> Execute()
        {
            return _selectedMapIdRepository.GetSelectedMapId()
                .ContinueWith(mapId => _mapsConfigurationRepository.GetMapConfigFor(mapId))
                .ContinueWith(mapConfiguration => _mapRepository.SaveCurrentMap(BuildGameMapFrom(mapConfiguration)));
        }

        private GameMap BuildGameMapFrom(MapConfigurationDto configDto)
        {
            return new GameMap(configDto.Id,
                configDto.Width, 
                configDto.Height, 
                configDto.SpawnPoints,
                configDto.BasePosition);
        }
    }
}