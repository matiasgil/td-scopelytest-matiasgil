﻿using TowerDefense.GameClock.Infrastructure.EventBus;

namespace TowerDefense.GameClock.Core.Actions
{
    public class OneSecondPassedAction
    {
        private readonly GameClockEventBus _gameClockEventBus;
        public OneSecondPassedAction(GameClockEventBus gameClockEventBus)
        {
            _gameClockEventBus = gameClockEventBus;
        }

        public void Execute(long secondsPassed)
        {
            _gameClockEventBus.OneSecondEmit(secondsPassed);
        }
    }
}