﻿using System;
using TowerDefense.GameClock.Infrastructure.Providers;
using TowerDefense.GameClock.Presentation;
using UniRx;
using UnityEngine;
using TMPro;

namespace TowerDefense.GameClock.UnityDelivery
{
    public class GameClockView : MonoBehaviour, IGameClockView
    {

        [SerializeField] private TextMeshProUGUI timer;
        public void Awake()
        {
            GameClockPresenterProvider.ProvideFor(this);
        }

        public void ShowTimer(long secondsElapsed)
        {
            var minutes = Mathf.Floor(secondsElapsed / 60).ToString("00");
            var seconds = (secondsElapsed % 60).ToString("00");

            timer.text = string.Format("{0}:{1}", minutes, seconds);
        }

        public void AddDisposables(IDisposable disposable)
        {
            disposable.AddTo(gameObject);
        }
    }
}