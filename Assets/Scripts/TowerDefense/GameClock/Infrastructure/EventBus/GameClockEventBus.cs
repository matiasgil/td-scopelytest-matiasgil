﻿using System;
using UniRx;

namespace TowerDefense.GameClock.Infrastructure.EventBus
{
    public class GameClockEventBus
    {
        private Subject<long> oneSecondIntervalSubject = new Subject<long>();
        public GameClockEventBus()
        {
            oneSecondIntervalSubject = new Subject<long>();
        }

        public void OneSecondEmit(long secondsPassed)
        {
            oneSecondIntervalSubject.OnNext(secondsPassed);
        }

        public IObservable<long> OneSecondObservable()
        {
            return oneSecondIntervalSubject;
        }
    }
}