﻿using TowerDefense.GameClock.Infrastructure.EventBus;
using Utils.Maybe;

namespace TowerDefense.GameClock.Infrastructure.Providers
{
    public static class GameClockEventBusProvider
    {
        private static Maybe<GameClockEventBus> _gameClockEventBus = Maybe<GameClockEventBus>.Nothing;
        public static GameClockEventBus ProvideEventBus()
        {
            _gameClockEventBus.DoWhenAbsent(() => _gameClockEventBus = new GameClockEventBus().ToMaybe());
            return _gameClockEventBus.Value;
        }
    }
}