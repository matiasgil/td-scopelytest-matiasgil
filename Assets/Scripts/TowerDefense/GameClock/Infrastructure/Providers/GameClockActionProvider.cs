﻿using TowerDefense.GameClock.Core.Actions;

namespace TowerDefense.GameClock.Infrastructure.Providers
{
    public static class GameClockActionProvider
    {
        public static OneSecondPassedAction ProvideOneSecondPassedAction()
        {
            return new OneSecondPassedAction(GameClockEventBusProvider.ProvideEventBus());
        }
    }
}