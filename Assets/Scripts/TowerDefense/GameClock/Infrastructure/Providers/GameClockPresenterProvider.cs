﻿using TowerDefense.GameClock.Presentation;
using TowerDefense.GameClock.UnityDelivery;

namespace TowerDefense.GameClock.Infrastructure.Providers
{
    public static class GameClockPresenterProvider
    {
        public static GameClockPresenter ProvideFor(GameClockView view)
        {
            return new GameClockPresenter(view, 
                GameClockActionProvider.ProvideOneSecondPassedAction());
        }
    }
}