﻿using System;
using TowerDefense.GameClock.Infrastructure.EventBus;
using TowerDefense.GameClock.Infrastructure.Providers;

namespace TowerDefense.GameClock
{
    public class GameClockModule
    {
        private readonly GameClockEventBus _gameClockEventBus;

        private GameClockModule()
        {
            _gameClockEventBus = GameClockEventBusProvider.ProvideEventBus();
        }

        public static GameClockModule Init()
        {
            return new GameClockModule();
        }

        public IObservable<long> GameClockOneSecondObservable()
        {
            return _gameClockEventBus.OneSecondObservable();
        }
    }
}