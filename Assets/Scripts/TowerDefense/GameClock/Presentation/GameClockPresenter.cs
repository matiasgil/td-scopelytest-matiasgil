﻿using System;
using TowerDefense.GameClock.Core.Actions;
using TowerDefense.GameClock.UnityDelivery;
using UniRx;

namespace TowerDefense.GameClock.Presentation
{
    public class GameClockPresenter
    {
        private readonly GameClockView _view;
        private readonly OneSecondPassedAction _oneSecondPassedAction;

        public GameClockPresenter(GameClockView view,
            OneSecondPassedAction oneSecondPassedAction)
        {
            _view = view;
            _oneSecondPassedAction = oneSecondPassedAction;

            //This could be started by an action from anywhere
            StartGameClock();
        }

        private void StartGameClock()
        {
            var disposable = Observable.Interval(TimeSpan.FromMilliseconds(1000))
                .Do(_oneSecondPassedAction.Execute)
                .Do(_view.ShowTimer)
                .Subscribe();            
            
            _view.AddDisposables(disposable);
        }
    }
}