﻿using System;

namespace TowerDefense.GameClock.Presentation
{
    public interface IGameClockView
    {
        public void ShowTimer(long secondsElapsed);
        public void AddDisposables(IDisposable disposable);
    }
}