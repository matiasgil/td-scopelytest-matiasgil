﻿using System;
using System.Collections.Generic;

namespace TowerDefense.Towers.Infrastructure.Repository
{
    public interface ISpawnerConfigRepository
    {
        IObservable<Dictionary<string, long>> GetTowersFor(string mapId);
    }
}