﻿using System;
using System.Collections.Generic;
using TowerDefense.Towers.Core.Domain.DTO;
using UniRx;
using Utils.Maybe;

namespace TowerDefense.Towers.Infrastructure.Repository.InMemory
{
    public class SpawnerConfigRepository : ISpawnerConfigRepository
    {
        private readonly Dictionary<string, Maybe<SpawnerConfigDto>> _spawnerConfigurations;
        public SpawnerConfigRepository()
        {
            _spawnerConfigurations = new Dictionary<string, Maybe<SpawnerConfigDto>>();
            var mockedSpawnerConfig = GetMockSpawnerConfig();
            _spawnerConfigurations.Add(mockedSpawnerConfig.MapId, mockedSpawnerConfig.ToMaybe());
        }

        public IObservable<Dictionary<string, long>> GetTowersFor(string mapId)
        {
            return Observable.Create<Dictionary<string, long>>(observer =>
            {
                _spawnerConfigurations[mapId].Do(config =>
                {
                    observer.OnNext(config.Towers);
                });
                
                observer.OnCompleted();
                
                return Disposable.Empty;
            });
        }

        private SpawnerConfigDto GetMockSpawnerConfig()
        {
            var towers = new Dictionary<string, long>
            {
                {"tower1", 5},
                {"tower2", 7}
            };

            return new SpawnerConfigDto("map_1", towers);
        }
    }
}