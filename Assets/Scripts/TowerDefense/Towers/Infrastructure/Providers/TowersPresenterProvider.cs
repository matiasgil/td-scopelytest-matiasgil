﻿using TowerDefense.Towers.Presentation;

namespace TowerDefense.Towers.Infrastructure.Providers
{
    public static class TowersPresenterProvider
    {
        public static TowerSpawnerPresenter ProvideFor(ITowerSpawnerView view)
        {
            return new TowerSpawnerPresenter(view,
                TowerActionProvider.ProvideCreateTowerSpawnerAction());
        }

        public static TowerPresenter ProvideFor(ITowerView view)
        {
            return new TowerPresenter(view);
        }
    }
}