﻿using TowerDefense.Towers.Infrastructure.Repository;
using TowerDefense.Towers.Infrastructure.Repository.InMemory;
using Utils.Maybe;

namespace TowerDefense.Towers.Infrastructure.Providers
{
    public static class TowerRepositoryProvider
    {
        private static Maybe<SpawnerConfigRepository> _spawnerConfigRepo = Maybe<SpawnerConfigRepository>.Nothing;  
        public static ISpawnerConfigRepository ProvideSpawnerConfigRepo()
        {
            _spawnerConfigRepo.DoWhenAbsent(() => _spawnerConfigRepo = new SpawnerConfigRepository().ToMaybe());
            return _spawnerConfigRepo.Value;
        }
    }
}