﻿using TowerDefense.Towers.Adapter;
using TowerDefense.Towers.Core.Actions;
using Utils;

namespace TowerDefense.Towers.Infrastructure.Providers
{
    public static class TowerActionProvider
    {
        public static CreateTowerSpawnerAction ProvideCreateTowerSpawnerAction()
        {
            return new CreateTowerSpawnerAction(TowerRepositoryProvider.ProvideSpawnerConfigRepo(),
                DependencyProvider.Get<ITowersMapAdapter>());
        }
    }
}