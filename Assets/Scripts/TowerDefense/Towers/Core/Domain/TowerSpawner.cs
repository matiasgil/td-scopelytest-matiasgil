﻿using System;
using System.Collections.Generic;
using UniRx;

namespace TowerDefense.Towers.Core.Domain
{
    public class TowerSpawner
    {
        private readonly Dictionary<string, long> _towerPrices;

        public TowerSpawner(Dictionary<string, long> towerPrices)
        {
            _towerPrices = towerPrices;
        }

        public IObservable<KeyValuePair<string, long>> TowersAvailable()
        {
            return Observable.Create<KeyValuePair<string, long>>(subject =>
            {
                foreach (var towerPrice in _towerPrices)
                {
                    subject.OnNext(towerPrice);
                }
                
                subject.OnCompleted();
                
                return Disposable.Empty;
            });
        }
    }
}