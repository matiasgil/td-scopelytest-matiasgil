﻿using System.Collections.Generic;

namespace TowerDefense.Towers.Core.Domain.DTO
{
    public struct SpawnerConfigDto
    {
        public string MapId;
        public Dictionary<string, long> Towers;

        public SpawnerConfigDto(string mapId, Dictionary<string, long> towers)
        {
            MapId = mapId;
            Towers = towers;
        }
    }
}