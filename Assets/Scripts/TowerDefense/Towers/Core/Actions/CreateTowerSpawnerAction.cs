﻿using System;
using TowerDefense.Towers.Adapter;
using TowerDefense.Towers.Core.Domain;
using TowerDefense.Towers.Infrastructure.Repository;
using UniRx;

namespace TowerDefense.Towers.Core.Actions
{
    public class CreateTowerSpawnerAction
    {
        private readonly ISpawnerConfigRepository _spawnerConfigRepo;
        private readonly ITowersMapAdapter _towersMapAdapter;

        public CreateTowerSpawnerAction(ISpawnerConfigRepository spawnerConfigRepo,
            ITowersMapAdapter towersMapAdapter)
        {
            _spawnerConfigRepo = spawnerConfigRepo;
            _towersMapAdapter = towersMapAdapter;
        }

        public IObservable<TowerSpawner> Execute()
        {
            return _towersMapAdapter.GetSelectedMap()
                .SelectMany(mapId => _spawnerConfigRepo.GetTowersFor(mapId))
                .Select(towerPrices => new TowerSpawner(towerPrices));
        }
    }
}