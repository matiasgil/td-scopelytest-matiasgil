﻿using System;

namespace TowerDefense.Towers.Adapter
{
    public interface ITowersMapAdapter
    {
        public IObservable<string> GetSelectedMap();
    }
}