﻿using TowerDefense.Towers.Adapter;
using Utils;

namespace TowerDefense.Towers
{
    public class TowersModule
    {
        private TowersModule(ITowersMapAdapter towersMapAdapter)
        {
            DependencyProvider.Set<ITowersMapAdapter>(towersMapAdapter);
        }

        public static TowersModule Init(ITowersMapAdapter towersMapAdapter)
        {
            return new TowersModule(towersMapAdapter);
        }
    }
}