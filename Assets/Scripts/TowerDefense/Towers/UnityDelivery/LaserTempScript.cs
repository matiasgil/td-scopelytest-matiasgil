using System;
using UniRx;
using UnityEngine;

namespace TowerDefense.Towers.UnityDelivery
{
    /// <summary>
    /// This is a temporal solution since i ran out of time.
    /// </summary>
    public class LaserTempScript : MonoBehaviour
    {
        private IDisposable disposable;

        private void OnEnable()
        {
            disposable = Observable.Interval(TimeSpan.FromMilliseconds(200))
                .Do(x => Disable())
                .Subscribe();
        }

        private void Disable()
        {
            disposable.Dispose();
            gameObject.SetActive(false);
        }
    }
}
