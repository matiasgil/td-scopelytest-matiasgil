﻿using System;
using TowerDefense.Enemies.UnityDelivery;
using TowerDefense.Towers.Presentation;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace TowerDefense.Towers.UnityDelivery
{
    public class UnityTowerView : MonoBehaviour, ITowerView
    {

        private GameObject _currentTarget;


        [SerializeField] private LineRenderer _lineRenderer;
        
        //This should be set through configuration.
        private int _turrentDamage = 5;

        private int _range = 6;
        /// <summary>
        /// Due to schedule reasons I cannot implement towers as intended on the architecture.
        /// I can explain for the test which would be the ideal way to implement it.
        /// 1- Enemies spawner should have a list of current alive mob with their current position.
        /// 2- If tower has no target, tower should ask through enemies adapter which is the closest to the tower considering a range.
        /// 3- Tower should apply damage to that enemy with a specific interval through enemies adapter.
        /// 4- Once enemy is dead, change to a new target.
        /// *** View should handle animations, projectiles, etc.
        /// </summary>
        public void Awake()
        {
            Observable.Interval(TimeSpan.FromMilliseconds(1000))
                .Do(ShootTarget)
                .Subscribe()
                .AddTo(gameObject);
        }

        private void ShootTarget(long totalShoots)
        {
            if (_currentTarget == null || EnemyIsOutOfRange())
            {
                GetNewTarget();
            }

            if (_currentTarget != null)
            {
                _lineRenderer.gameObject.SetActive(true);
                _lineRenderer.SetPosition(0, transform.position + new Vector3(0, 1, 0));
                _lineRenderer.SetPosition(1, _currentTarget.transform.position);
                _currentTarget.GetComponent<UnityEnemyView>().ApplyDamage(_turrentDamage);
            }
        }

        private bool EnemyIsOutOfRange()
        {
            if (_currentTarget == null) return true;
            return Vector3.Distance(_currentTarget.transform.position, transform.position) > _range;
        }

        
        //This is not a final solution, obviously this does not scale.
        //With ideal solution implementing enemies adapter this would be way more efficient.
        private void GetNewTarget()
        {
            var enemies = GameObject.FindGameObjectsWithTag("Enemy");
            
            var closestDistance = (float)_range;

            foreach (var enemy in enemies)
            {
                var distance = Vector3.Distance(transform.position, enemy.transform.position);
                if (distance <= _range && distance < closestDistance)
                {
                    _currentTarget = enemy.gameObject;
                    closestDistance = distance;
                }
            }
        }
    }
}