﻿using System;
using System.Collections.Generic;
using TMPro;
using TowerDefense.Towers.Infrastructure.Providers;
using TowerDefense.Towers.Presentation;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace TowerDefense.Towers.UnityDelivery
{
    public class UnityTowerSpawnerView : MonoBehaviour, ITowerSpawnerView
    {
        [SerializeField] private List<TowerViewConfig> towerConfigs = new List<TowerViewConfig>();
        private readonly Dictionary<string, GameObject> _towerPrefabs = new Dictionary<string, GameObject>();
        

        [SerializeField] private GameObject towerButtonPrefab;
        private readonly Dictionary<string, Button> _towerButtons = new Dictionary<string, Button>();

        [SerializeField] private RectTransform towerButtonsParent;

        [SerializeField] private GameObject towerPlacerUI;

        private bool _placingTower = false;
        private string _placingTowerId = string.Empty;
        private Vector3 _towerPlacementTargetPosition = Vector3.zero;

        public event Action<string> BuyTower = x => { };

        private void Awake()
        {
            TowersPresenterProvider.ProvideFor(this);
            
            foreach (var towerConfig in towerConfigs)
            {
                _towerPrefabs.Add(towerConfig.Id, towerConfig.Prefab);
            }
        }

        private void FixedUpdate()
        {
            HandleTowerPlacing();
        }
        
        public void ShowTowerButton(KeyValuePair<string, long> tower)
        {
            var towerButton = Instantiate(towerButtonPrefab, towerButtonsParent).GetComponent<Button>();

            towerButton.GetComponentInChildren<Text>().text = $"{tower.Key} : $ {tower.Value}";
            
            towerButton.onClick.AsObservable()
                .Do(unit => BuyTower(tower.Key))
                .Subscribe()
                .AddTo(towerButton.gameObject);
            
            _towerButtons.Add(tower.Key, towerButton);
        }

        public void StartPlacingTower(string towerId)
        {
            DisableTowerButtons();
            _placingTower = true;
            towerPlacerUI.SetActive(true);
            _placingTowerId = towerId;
        }

        public void DisposeWhenDestroyed(IDisposable disposable)
        {
            disposable.AddTo(gameObject);
        }
        
        private void DisableTowerButtons()
        {
            foreach (var towerButton in _towerButtons)
            {
                towerButton.Value.interactable = false;
            }
        }

        private void EnableTowerButtons()
        {
            foreach (var towerButton in _towerButtons)
            {
                towerButton.Value.interactable = true;
            }
        }

        private Ray _ray;
        private RaycastHit _raycastHit;
        
        private void HandleTowerPlacing()
        {
            if (!_placingTower) return;

            if (Camera.main != null)
            {
                _ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(_ray, out _raycastHit))
                {
                    if (_raycastHit.transform.CompareTag("Map"))
                    {
                        towerPlacerUI.transform.position = new Vector3(_raycastHit.point.x, 0.35f, _raycastHit.point.z);
                        _towerPlacementTargetPosition = _raycastHit.point;
                    }
                }
            }

            if (Input.GetMouseButtonDown(0))
            {
                PlaceTower();
            }
        }

        private void PlaceTower()
        {
            _placingTower = false;
            towerPlacerUI.SetActive(false);
            EnableTowerButtons();

            Instantiate(_towerPrefabs[_placingTowerId], _towerPlacementTargetPosition, Quaternion.identity);
        }
    }

    [Serializable]
    public struct TowerViewConfig
    {
        public string Id;
        public GameObject Prefab;
    }
}