﻿using TowerDefense.Towers.Core.Actions;
using TowerDefense.Towers.Core.Domain;
using UniRx;

namespace TowerDefense.Towers.Presentation
{
    public class TowerSpawnerPresenter
    {
        private readonly ITowerSpawnerView _view;
        private readonly CreateTowerSpawnerAction _createTowerSpawnerAction;

        public TowerSpawnerPresenter(ITowerSpawnerView view, 
            CreateTowerSpawnerAction createTowerSpawnerAction)
        {
            _view = view;
            _createTowerSpawnerAction = createTowerSpawnerAction;

            _createTowerSpawnerAction.Execute()
                .Do(SetUpTowersUI)
                .Subscribe()
                .Dispose();

            _view.BuyTower += BuyTower;
        }

        private void BuyTower(string towerId)
        {
            //TODO: affect economy through adapter
            _view.StartPlacingTower(towerId);
        }

        private void SetUpTowersUI(TowerSpawner towerSpawner)
        {
            towerSpawner.TowersAvailable()
                .Do(_view.ShowTowerButton)
                .Subscribe()
                .Dispose();
        }
    }
}