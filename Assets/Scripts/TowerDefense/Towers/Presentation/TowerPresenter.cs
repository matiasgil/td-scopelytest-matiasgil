﻿namespace TowerDefense.Towers.Presentation
{
    public class TowerPresenter
    {
        private readonly ITowerView _view;

        public TowerPresenter(ITowerView view)
        {
            _view = view;
        }
    }
}