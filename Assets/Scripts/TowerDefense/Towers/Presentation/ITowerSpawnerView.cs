﻿using System;
using System.Collections.Generic;

namespace TowerDefense.Towers.Presentation
{
    public interface ITowerSpawnerView
    {
        event Action<string> BuyTower;
        public void DisposeWhenDestroyed(IDisposable disposable);
        void ShowTowerButton(KeyValuePair<string, long> tower);
        void StartPlacingTower(string towerId);
    }
}