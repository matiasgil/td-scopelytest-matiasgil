﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace TowerDefense.GameLevel
{
    public class GameLevelModule
    {
        private int tempEnemiesCounter = 0;
        private GameLevelModule()
        {
            
        }

        public static GameLevelModule Init()
        {
            return new GameLevelModule();
        }

        public void NewGame()
        {
            tempEnemiesCounter = 0;
        }

        public void EnemyEnteredTheBase()
        {
            tempEnemiesCounter++;

            if (tempEnemiesCounter == 3)
            {
                SceneManager.LoadScene("Home");
            }
        }

        public void Won()
        {
            SceneManager.LoadScene("Won");
        }
    }
}