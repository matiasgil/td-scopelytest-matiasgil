﻿using TowerDefense.GameLevel;

namespace TowerDefense.Shared.GameLevel
{
    public static class GameLevelDependencies
    {
        public static GameLevelModule Set()
        {
            return GameLevelModule.Init();
        }
    }
}