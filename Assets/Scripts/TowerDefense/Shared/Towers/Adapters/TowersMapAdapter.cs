﻿using System;
using TowerDefense.Map;
using TowerDefense.Towers.Adapter;

namespace TowerDefense.Shared.Towers.Adapters
{
    public class TowersMapAdapter : ITowersMapAdapter
    {
        private readonly MapModule _mapModule;

        public TowersMapAdapter(MapModule mapModule)
        {
            _mapModule = mapModule;
        }

        public IObservable<string> GetSelectedMap()
        {
            return _mapModule.GetSelectedMap();
        }
    }
}