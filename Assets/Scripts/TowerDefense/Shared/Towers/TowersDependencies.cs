﻿using TowerDefense.Map;
using TowerDefense.Shared.Towers.Adapters;
using TowerDefense.Towers;
using Utils;

namespace TowerDefense.Shared.Towers
{
    public static class TowersDependencies
    {
        public static TowersModule Set()
        {
            var towersMapAdapter = new TowersMapAdapter(DependencyProvider.Get<MapModule>());
            return TowersModule.Init(towersMapAdapter);
        }
    }
}