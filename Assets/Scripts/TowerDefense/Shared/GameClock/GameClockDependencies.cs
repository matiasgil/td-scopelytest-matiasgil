﻿using TowerDefense.GameClock;

namespace TowerDefense.Shared.GameClock
{
    public class GameClockDependencies
    {
        public static GameClockModule Set()
        {
            return GameClockModule.Init();
        }
    }
}