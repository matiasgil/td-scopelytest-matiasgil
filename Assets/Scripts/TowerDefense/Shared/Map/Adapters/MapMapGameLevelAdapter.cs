﻿using TowerDefense.GameLevel;
using TowerDefense.Map.Adapter;

namespace TowerDefense.Shared.Map.Adapters
{
    public class MapMapGameLevelAdapter : IMapGameLevelAdapter
    {
        private readonly GameLevelModule _gameLevelModule;

        public MapMapGameLevelAdapter(GameLevelModule gameLevelModule)
        {
            _gameLevelModule = gameLevelModule;
        }

        public void NewGame()
        {
            _gameLevelModule.NewGame();
        }

        public void EnemyEnteredTheBase()
        {
            _gameLevelModule.EnemyEnteredTheBase();
        }
    }
}