﻿using TowerDefense.GameLevel;
using TowerDefense.Map;
using TowerDefense.Shared.Map.Adapters;
using Utils;

namespace TowerDefense.Shared.Map
{
    public static class MapDependencies
    {
        public static MapModule Set()
        {
            var gameLevelAdapter = new MapMapGameLevelAdapter(DependencyProvider.Get<GameLevelModule>());
            return MapModule.Init(gameLevelAdapter);
        }
    }
}