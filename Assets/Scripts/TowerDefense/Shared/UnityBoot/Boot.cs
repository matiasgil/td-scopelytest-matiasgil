﻿using System;
using System.Collections;
using System.Collections.Generic;
using TowerDefense.Enemies;
using TowerDefense.GameClock;
using TowerDefense.GameLevel;
using TowerDefense.Home;
using TowerDefense.Map;
using TowerDefense.Shared.Enemies;
using TowerDefense.Shared.GameClock;
using TowerDefense.Shared.GameLevel;
using TowerDefense.Shared.Home;
using TowerDefense.Shared.Map;
using TowerDefense.Shared.Towers;
using TowerDefense.Towers;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace TowerDefense.Shared.UnityBoot
{
    public class Boot : MonoBehaviour
    {

        [SerializeField] private Slider loadBar;

        private List<Action> loadingModulesDelegates = new List<Action>();
        
        IEnumerator Start()
        {
            SetUp();

            yield return LoadModules();

            SceneManager.LoadScene("Home");
        }

        private IEnumerator LoadModules()
        {
            var maxLoadingModules = loadingModulesDelegates.Count;
            for (int i = 0; i < loadingModulesDelegates.Count; i++)
            {
                loadingModulesDelegates[i].Invoke();

                loadBar.value = GetLoadingBarValue(i, maxLoadingModules);
                
                yield return new WaitForEndOfFrame();
            }
        }

        private void SetUp()
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
            
            loadingModulesDelegates.Add(LoadGameLevelModuleDependecies);
            loadingModulesDelegates.Add(LoadMapModuleDependencies);
            loadingModulesDelegates.Add(LoadHomeModuleDependencies);
            loadingModulesDelegates.Add(LoadGameGlockModuleDependencies);
            loadingModulesDelegates.Add(LoadEnemiesModuleDependencies);
            loadingModulesDelegates.Add(LoadTowersModuleDependencies);
        }
        
        private void LoadGameLevelModuleDependecies()
        {
            var gameLevelModule = GameLevelDependencies.Set();
            DependencyProvider.Set<GameLevelModule>(gameLevelModule);
        }

        private void LoadMapModuleDependencies()
        {
            var mapModule = MapDependencies.Set();
            DependencyProvider.Set<MapModule>(mapModule);
        }

        private void LoadHomeModuleDependencies()
        {
            var homeModule = HomeDependencies.Set();
            DependencyProvider.Set<HomeModule>(homeModule);
        }

        private void LoadGameGlockModuleDependencies()
        {
            var gameClockModule = GameClockDependencies.Set();
            DependencyProvider.Set<GameClockModule>(gameClockModule);
        }

        private void LoadEnemiesModuleDependencies()
        {
            var enemiesModule = EnemiesDependencies.Set();
            DependencyProvider.Set<EnemiesModule>(enemiesModule);
        }
        
        private void LoadTowersModuleDependencies()
        {
            var towersModule = TowersDependencies.Set();
            DependencyProvider.Set<TowersModule>(towersModule);
        }

        private float GetLoadingBarValue(int currentLoadedModule, int maxModules)
        {
            return currentLoadedModule * 100f / maxModules / 100f;
        }
    }
}