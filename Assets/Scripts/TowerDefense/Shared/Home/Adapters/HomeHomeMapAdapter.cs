﻿using System;
using TowerDefense.Home.Adapter;
using TowerDefense.Map;

namespace TowerDefense.Shared.Home.Adapters
{
    public class HomeHomeMapAdapter : IHomeMapAdapter
    {
        private readonly MapModule _mapModule;

        public HomeHomeMapAdapter(MapModule mapModule)
        {
            _mapModule = mapModule;
        }

        public IObservable<string> SelectMap(string mapId)
        {
            return _mapModule.SetMapId(mapId);
        }
    }
}