﻿using TowerDefense.Home;
using TowerDefense.Map;
using TowerDefense.Shared.Home.Adapters;
using UniRx;
using Utils;

namespace TowerDefense.Shared.Home
{
    public static class HomeDependencies
    {
        public static HomeModule Set()
        {
            var mapSelectionAdapter = new HomeHomeMapAdapter(DependencyProvider.Get<MapModule>());
            return HomeModule.Init(mapSelectionAdapter);
        }
    }
}