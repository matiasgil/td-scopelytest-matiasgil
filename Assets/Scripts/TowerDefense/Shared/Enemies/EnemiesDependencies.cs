﻿using TowerDefense.Enemies;
using TowerDefense.GameClock;
using TowerDefense.GameLevel;
using TowerDefense.Map;
using TowerDefense.Shared.Enemies.Adapters;
using Utils;

namespace TowerDefense.Shared.Enemies
{
    public static class EnemiesDependencies
    {
        public static EnemiesModule Set()
        {
            var gameClockAdapter = new EnemiesEnemiesGameClockAdapter(DependencyProvider.Get<GameClockModule>());
            var mapAdapter = new EnemiesEnemiesMapAdapter(DependencyProvider.Get<MapModule>());
            var gameLevelAdapter = new EnemiesGameLevelAdapter(DependencyProvider.Get<GameLevelModule>());
            return EnemiesModule.Init(gameClockAdapter,
                mapAdapter,
                gameLevelAdapter);
        }
    }
}