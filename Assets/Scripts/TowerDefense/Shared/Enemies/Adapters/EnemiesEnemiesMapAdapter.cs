﻿using System;
using TowerDefense.Enemies.Adapter;
using TowerDefense.Map;
using Utils;

namespace TowerDefense.Shared.Enemies.Adapters
{
    public class EnemiesEnemiesMapAdapter : IEnemiesMapAdapter
    {
        private readonly MapModule _mapModule;

        public EnemiesEnemiesMapAdapter(MapModule mapModule)
        {
            _mapModule = mapModule;
        }

        public IObservable<string> GetSelectedMap()
        {
            return _mapModule.GetSelectedMap();
        }

        public IObservable<PlanePosition> GetMapSpawnPoint()
        {
            return _mapModule.GetMapSpawnPoint();
        }

        public IObservable<PlanePosition> GetMapBasePoint()
        {
            return _mapModule.GetMapBasePoint();
        }
    }
}