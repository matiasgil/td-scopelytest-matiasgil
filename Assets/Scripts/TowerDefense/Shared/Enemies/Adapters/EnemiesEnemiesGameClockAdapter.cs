﻿using System;
using TowerDefense.Enemies.Adapter;
using TowerDefense.GameClock;

namespace TowerDefense.Shared.Enemies.Adapters
{
    public class EnemiesEnemiesGameClockAdapter : IEnemiesGameClockAdapter
    {
        private readonly GameClockModule _gameClockModule;

        public EnemiesEnemiesGameClockAdapter(GameClockModule gameClockModule)
        {
            _gameClockModule = gameClockModule;
        }

        public IObservable<long> GameClockOneSecondObservable()
        {
            return _gameClockModule.GameClockOneSecondObservable();
        }
    }
}