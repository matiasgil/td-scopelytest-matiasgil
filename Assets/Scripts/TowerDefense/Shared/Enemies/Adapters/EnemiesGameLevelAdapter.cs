﻿using TowerDefense.Enemies.Adapter;
using TowerDefense.GameLevel;

namespace TowerDefense.Shared.Enemies.Adapters
{
    public class EnemiesGameLevelAdapter : IEnemiesGameLevelAdapter
    {
        private readonly GameLevelModule _gameLevelModule;

        public EnemiesGameLevelAdapter(GameLevelModule gameLevelModule)
        {
            _gameLevelModule = gameLevelModule;
        }

        public void Won()
        {
            _gameLevelModule.Won();
        }
    }
}