﻿using System;
using TowerDefense.Enemies.Infrastructure.Providers;
using TowerDefense.Enemies.Presentation;
using UniRx;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Utils;

namespace TowerDefense.Enemies.UnityDelivery
{
    public class UnityEnemyView : MonoBehaviour, IEnemyView
    {
        [SerializeField] private string enemyId;
        [SerializeField] private GameObject healthBar;
        [SerializeField] private NavMeshAgent navMeshAgent;
        private Slider _healthBarSlider;

        public event Action<int> DamageReceived = dmg => { };
        private void Awake()
        {
            EnemiesPresenterProvider.ProvideFor(this, enemyId);
            _healthBarSlider = healthBar.GetComponentInChildren<Slider>();
        }

        private void LateUpdate()
        {
            healthBar.transform.LookAt(healthBar.transform.position + Camera.main.transform.forward);
        }

        public void DisposeWhenDestroyed(IDisposable disposable)
        {
            disposable.AddTo(gameObject);
        }

        public void MoveToSpawnPoint(PlanePosition position)
        {
            transform.localPosition = new Vector3(position.X, 0, position.Y);
        }

        public void UpdateHealth(int currentHealth)
        {
            _healthBarSlider.value = currentHealth;
            
            if(currentHealth == 0)
                Destroy(gameObject);
        }

        public void SetSpeed(float speed)
        {
            navMeshAgent.speed = speed;
        }

        public void SetDestination(PlanePosition planePosition)
        {
            navMeshAgent.SetDestination(transform.TransformPoint(new Vector3(planePosition.X*2, 0, planePosition.Y*2)));
        }

        public void ApplyDamage(int turretDamage)
        {
            DamageReceived.Invoke(turretDamage);
        }
    }
}