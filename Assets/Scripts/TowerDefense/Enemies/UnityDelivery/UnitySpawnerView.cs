using System;
using System.Collections.Generic;
using TowerDefense.Enemies.Infrastructure.Providers;
using TowerDefense.Enemies.Presentation;
using UniRx;
using UnityEngine;

namespace TowerDefense.Enemies.UnityDelivery
{
    public class UnitySpawnerView : MonoBehaviour, ISpawnerView
    {
        [SerializeField] private Transform enemiesParent;
        [SerializeField] private List<EnemyViewConfig> enemyPrefabsConfig = new List<EnemyViewConfig>();
        
        private readonly Dictionary<string, GameObject> _enemyPrefabs = new Dictionary<string, GameObject>();

        private void Awake()
        {
            EnemiesPresenterProvider.ProvideFor(this);

            foreach (var enemyPrefabConfig in enemyPrefabsConfig)
            {
                _enemyPrefabs.Add(enemyPrefabConfig.id, enemyPrefabConfig.prefab);
            }
        }

        private void SpawnEnemy(string id)
        {
            //TODO: implement a dynamic pool of enemies instead of directly instantiation, but for time reasons i will leave it as a TODO
            Instantiate(_enemyPrefabs[id], enemiesParent);
        }

        public void SpawnEnemies(string[] enemiesId)
        {
            foreach (var enemyId in enemiesId)
            {
                SpawnEnemy(enemyId);
            }
        }

        public void DisposeWhenDestroyed(IDisposable disposable)
        {
            disposable.AddTo(gameObject);
        }
    }

    [Serializable]
    public struct EnemyViewConfig
    {
        public string id;
        public GameObject prefab;
    }
}