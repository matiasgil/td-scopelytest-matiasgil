﻿using System;
using TowerDefense.Enemies.Adapter;
using TowerDefense.Enemies.Core.Domain;
using TowerDefense.Enemies.Core.Domain.DTO;
using TowerDefense.Enemies.Infrastructure.Repository;
using UniRx;

namespace TowerDefense.Enemies.Core.Actions
{
    public class CreateSpawnerAction
    {
        private readonly ISpawnerConfigurationRepository _spawnerConfigurationRepository;
        private readonly IEnemiesGameClockAdapter _enemiesGameClockAdapter;
        private readonly IEnemiesMapAdapter _enemiesMapAdapter;
        private readonly IEnemiesGameLevelAdapter _gameLevelAdapter;

        public CreateSpawnerAction(ISpawnerConfigurationRepository spawnerConfigurationRepository,
            IEnemiesGameClockAdapter enemiesGameClockAdapter,
            IEnemiesMapAdapter enemiesMapAdapter,
            IEnemiesGameLevelAdapter gameLevelAdapter)
        {
            _spawnerConfigurationRepository = spawnerConfigurationRepository;
            _enemiesGameClockAdapter = enemiesGameClockAdapter;
            _enemiesMapAdapter = enemiesMapAdapter;
            _gameLevelAdapter = gameLevelAdapter;
        }

        public IObservable<Spawner> Execute()
        {
            return _enemiesMapAdapter.GetSelectedMap().ContinueWith(_spawnerConfigurationRepository.GetSpawnerConfigFor)
                .Select(CreateSpawner);
        }

        private Spawner CreateSpawner(SpawnerConfigurationDto config)
        {
            return new Spawner(config.MapId, 
                config.EnemiesSpawnBySecond, 
                _enemiesGameClockAdapter,
                _gameLevelAdapter);
        }
    }
}