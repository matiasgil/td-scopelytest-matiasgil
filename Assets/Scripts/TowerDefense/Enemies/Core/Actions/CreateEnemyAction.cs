﻿using System;
using TowerDefense.Enemies.Core.Domain;
using TowerDefense.Enemies.Infrastructure.Providers;
using TowerDefense.Enemies.Infrastructure.Repository;
using UniRx;

namespace TowerDefense.Enemies.Core.Actions
{
    public class CreateEnemyAction
    {
        private readonly IEnemiesConfigurationRepository _provideEnemiesConfigurationRepository;

        public CreateEnemyAction(IEnemiesConfigurationRepository provideEnemiesConfigurationRepository)
        {
            _provideEnemiesConfigurationRepository = provideEnemiesConfigurationRepository;
        }

        public IObservable<Enemy> Execute(string enemyId)
        {
            return _provideEnemiesConfigurationRepository.GetEnemyConfigFor(enemyId)
                .Select(config => new Enemy(config.Health, config.Speed));
        }
    }
}