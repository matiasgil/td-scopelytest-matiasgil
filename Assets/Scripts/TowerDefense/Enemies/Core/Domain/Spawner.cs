﻿using System;
using System.Collections.Generic;
using TowerDefense.Enemies.Adapter;
using UniRx;
using Utils.Maybe;

namespace TowerDefense.Enemies.Core.Domain
{
    public class Spawner
    {
        private readonly string _mapId;
        private readonly Dictionary<long, string[]> _config;
        private readonly IEnemiesGameClockAdapter _enemiesGameClockAdapter;
        private readonly IEnemiesGameLevelAdapter _gameLevelAdapter;

        public Spawner(string mapId,
            Dictionary<long, string[]> config,
            IEnemiesGameClockAdapter enemiesGameClockAdapter,
            IEnemiesGameLevelAdapter gameLevelAdapter)
        {
            _mapId = mapId;
            _config = config;
            _enemiesGameClockAdapter = enemiesGameClockAdapter;
            _gameLevelAdapter = gameLevelAdapter;
        }

        public IObservable<Maybe<string[]>> SpawnEnemies()
        {
            return _enemiesGameClockAdapter.GameClockOneSecondObservable()
                .Do(CheckIfLevelEnded)
                .Select(GetEnemiesToSpawn);
        }

        
        //TEMP SOLUTION DUE TO OUT OF TIME
        private void CheckIfLevelEnded(long secondsPassed)
        {
            if (secondsPassed > 120)
            {
                _gameLevelAdapter.Won();
            }
        }

        private Maybe<string[]> GetEnemiesToSpawn(long seconds)
        {
            return _config.ContainsKey(seconds) ? _config[seconds].ToMaybe() : Maybe<string[]>.Nothing;
        }
    }
}