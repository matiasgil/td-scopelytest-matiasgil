﻿namespace TowerDefense.Enemies.Core.Domain.DTO
{
    public struct EnemyConfigDto
    {
        public readonly int Health;
        public readonly float Speed;

        public EnemyConfigDto(int health, float speed)
        {
            Health = health;
            Speed = speed;
        }
    }
}