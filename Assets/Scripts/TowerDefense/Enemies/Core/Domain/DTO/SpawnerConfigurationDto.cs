﻿using System.Collections.Generic;

namespace TowerDefense.Enemies.Core.Domain.DTO
{
    public readonly struct SpawnerConfigurationDto
    {
        public readonly string MapId;
        public readonly Dictionary<long, string[]> EnemiesSpawnBySecond;

        public SpawnerConfigurationDto(string mapId,
            Dictionary<long, string[]> enemiesSpawnBySecond)
        {
            MapId = mapId;
            EnemiesSpawnBySecond = enemiesSpawnBySecond;
        }
    }
}