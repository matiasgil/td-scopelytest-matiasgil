﻿using System;
using UniRx;

namespace TowerDefense.Enemies.Core.Domain
{
    public class Enemy
    {
        private readonly int _totalHealth;
        private readonly float _speed;

        private int _totalDamageReceived = 0;

        private readonly Subject<int> _currentHealthSubject = new Subject<int>();

        public Enemy(int totalHealth, float speed)
        {
            _totalHealth = totalHealth;
            _speed = speed;
        }

        public float Speed()
        {
            return _speed;
        }

        public IObservable<int> CurrentHealth()
        {
            return _currentHealthSubject;
        }

        public void ApplyDamage(int damage)
        {
            _totalDamageReceived += damage;

            var currentHealth = _totalHealth - _totalDamageReceived;
            if (currentHealth < 0)
                currentHealth = 0;

            var percentageHealth = currentHealth * 100 / _totalHealth;
            
            _currentHealthSubject.OnNext(percentageHealth);
        }
    }
}