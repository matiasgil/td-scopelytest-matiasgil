﻿using TowerDefense.Enemies.Adapter;
using Utils;

namespace TowerDefense.Enemies
{
    public class EnemiesModule
    {
        private readonly IEnemiesGameLevelAdapter _enemiesGameLevelAdapter;

        private EnemiesModule(IEnemiesGameClockAdapter enemiesGameClockAdapter, 
            IEnemiesMapAdapter enemiesMapAdapter,
            IEnemiesGameLevelAdapter enemiesGameLevelAdapter)
        {
            DependencyProvider.Set<IEnemiesGameClockAdapter>(enemiesGameClockAdapter);
            DependencyProvider.Set<IEnemiesMapAdapter>(enemiesMapAdapter);
            DependencyProvider.Set<IEnemiesGameLevelAdapter>(enemiesGameLevelAdapter);
        }

        public static EnemiesModule Init(IEnemiesGameClockAdapter enemiesGameClockAdapter,
            IEnemiesMapAdapter enemiesMapAdapter, IEnemiesGameLevelAdapter enemiesGameLevelAdapter)
        {
            return new EnemiesModule(enemiesGameClockAdapter, 
                enemiesMapAdapter,
                enemiesGameLevelAdapter);
        }
    }
}