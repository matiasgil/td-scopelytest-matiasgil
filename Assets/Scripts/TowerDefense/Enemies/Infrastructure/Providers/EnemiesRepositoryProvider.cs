﻿using TowerDefense.Enemies.Infrastructure.Repository;
using TowerDefense.Enemies.Infrastructure.Repository.InMemory;
using Utils.Maybe;

namespace TowerDefense.Enemies.Infrastructure.Providers
{
    public static class EnemiesRepositoryProvider
    {
        private static Maybe<SpawnerConfigurationRepository> _spawnConfigRepository = Maybe<SpawnerConfigurationRepository>.Nothing;

        public static ISpawnerConfigurationRepository ProvideSpawnerConfigurationRepository()
        {
            _spawnConfigRepository.DoWhenAbsent(() => _spawnConfigRepository = new SpawnerConfigurationRepository().ToMaybe());
            return _spawnConfigRepository.Value;
        }

        private static Maybe<EnemiesConfigurationRepository> _enemiesConfigRepository = Maybe<EnemiesConfigurationRepository>.Nothing;
        public static IEnemiesConfigurationRepository ProvideEnemiesConfigurationRepository()
        {
            _enemiesConfigRepository.DoWhenAbsent(() => _enemiesConfigRepository = new EnemiesConfigurationRepository().ToMaybe());
            return _enemiesConfigRepository.Value;
        }
    }
}