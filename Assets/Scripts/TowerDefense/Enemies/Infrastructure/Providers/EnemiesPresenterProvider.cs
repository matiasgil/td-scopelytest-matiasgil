﻿using TowerDefense.Enemies.Adapter;
using TowerDefense.Enemies.Presentation;
using Utils;

namespace TowerDefense.Enemies.Infrastructure.Providers
{
    public static class EnemiesPresenterProvider
    {
        public static SpawnerPresenter ProvideFor(ISpawnerView view)
        {
            return new SpawnerPresenter(view,
                EnemiesActionProvider.ProvideCreteSpawner());
        }
        
        public static EnemyPresenter ProvideFor(IEnemyView view, string enemyId)
        {
            return new EnemyPresenter(view, 
                enemyId,
                DependencyProvider.Get<IEnemiesMapAdapter>(),
                EnemiesActionProvider.ProvideCreateEnemyAction());
        }
    }
}