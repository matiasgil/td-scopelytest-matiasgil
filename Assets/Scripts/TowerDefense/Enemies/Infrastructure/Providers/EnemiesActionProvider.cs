﻿using TowerDefense.Enemies.Adapter;
using TowerDefense.Enemies.Core.Actions;
using TowerDefense.Enemies.Presentation;
using Utils;

namespace TowerDefense.Enemies.Infrastructure.Providers
{
    public static class EnemiesActionProvider
    {
        public static CreateSpawnerAction ProvideCreteSpawner()
        {
            return new CreateSpawnerAction(EnemiesRepositoryProvider.ProvideSpawnerConfigurationRepository(),
                DependencyProvider.Get<IEnemiesGameClockAdapter>(),
                DependencyProvider.Get<IEnemiesMapAdapter>(),
                DependencyProvider.Get<IEnemiesGameLevelAdapter>());
        }

        public static CreateEnemyAction ProvideCreateEnemyAction()
        {
            return new CreateEnemyAction(EnemiesRepositoryProvider.ProvideEnemiesConfigurationRepository());
        }
    }
}