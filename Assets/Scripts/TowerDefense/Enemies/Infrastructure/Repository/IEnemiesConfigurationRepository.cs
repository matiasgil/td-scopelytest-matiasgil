﻿using System;
using TowerDefense.Enemies.Core.Domain.DTO;

namespace TowerDefense.Enemies.Infrastructure.Repository
{
    public interface IEnemiesConfigurationRepository
    {
        public IObservable<EnemyConfigDto> GetEnemyConfigFor(string enemyId);
    }
}