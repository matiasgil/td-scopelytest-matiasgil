﻿using System;
using System.Collections.Generic;
using TowerDefense.Enemies.Core.Domain.DTO;
using UniRx;
using Utils.Maybe;

namespace TowerDefense.Enemies.Infrastructure.Repository.InMemory
{
    public class SpawnerConfigurationRepository : ISpawnerConfigurationRepository
    {

        private readonly Dictionary<string, Maybe<SpawnerConfigurationDto>> _spawnerConfigurations;

        /// <summary>
        /// Since this is a technical test¿ enemies configuration is hardcoded on this Repo implementation,
        /// but it could be replaced for a repository that fetches data from a backend.
        /// </summary>
        public SpawnerConfigurationRepository()
        {
            _spawnerConfigurations = new Dictionary<string, Maybe<SpawnerConfigurationDto>>();
            var mockedSpawnerConfig = GetMockSpawnerConfig();
            _spawnerConfigurations.Add(mockedSpawnerConfig.MapId, mockedSpawnerConfig.ToMaybe());
            
        }

        public IObservable<SpawnerConfigurationDto> GetSpawnerConfigFor(string mapId)
        {
            return Observable.Create<SpawnerConfigurationDto>(observer =>
            {
                _spawnerConfigurations[mapId]
                    .Do(observer.OnNext)
                    .DoWhenAbsent(() => observer.OnError(new Exception("There is no spawns config for that MapID")));
                
                observer.OnCompleted();

                return Disposable.Empty;
            });
        }

        private SpawnerConfigurationDto GetMockSpawnerConfig()
        {
            Dictionary<long, string[]> enemiesSpawnBySecond = new Dictionary<long, string[]>();

            string[] a = {"enemy1", "enemy2"};
            enemiesSpawnBySecond.Add(10, a);
            
            string[] b = {"enemy1", "enemy1", "enemy2", "enemy2"};
            enemiesSpawnBySecond.Add(20, b);
            
            string[] c = {"enemy1", "enemy1", "enemy1", "enemy2", "enemy2", "enemy2"};
            enemiesSpawnBySecond.Add(35, c);
            
            string[] d = {"enemy1", "enemy1", "enemy1", "enemy2", "enemy2", "enemy2"};
            enemiesSpawnBySecond.Add(55, d);
            
            string[] e = {"enemy1", "enemy1", "enemy1", "enemy2", "enemy2", "enemy2"};
            enemiesSpawnBySecond.Add(70, e);
            
            string[] f = {"enemy1", "enemy1", "enemy1", "enemy2", "enemy2", "enemy2"};
            enemiesSpawnBySecond.Add(90, f);
            
            string[] g = {"enemy1", "enemy1", "enemy1", "enemy2", "enemy2", "enemy2"};
            enemiesSpawnBySecond.Add(100, g);

            return new SpawnerConfigurationDto("map_1", enemiesSpawnBySecond);
        }
    }
}