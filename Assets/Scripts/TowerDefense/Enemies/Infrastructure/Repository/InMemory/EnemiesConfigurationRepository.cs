﻿using System;
using System.Collections.Generic;
using TowerDefense.Enemies.Core.Domain.DTO;
using UniRx;
using Utils.Maybe;

namespace TowerDefense.Enemies.Infrastructure.Repository.InMemory
{
    public class EnemiesConfigurationRepository : IEnemiesConfigurationRepository
    {
        private readonly Dictionary<string, Maybe<EnemyConfigDto>> _enemiesConfigurations;
        
        /// <summary>
        /// Since this is a technical test¿ enemies configuration is hardcoded on this Repo implementation,
        /// but it could be replaced for a repository that fetches data from a backend.
        /// </summary>
        public EnemiesConfigurationRepository()
        {
            _enemiesConfigurations = new Dictionary<string, Maybe<EnemyConfigDto>>();
            _enemiesConfigurations = GetMockEnemiesConfig();
        }
        
        public IObservable<EnemyConfigDto> GetEnemyConfigFor(string enemyId)
        {
            return Observable.Create<EnemyConfigDto>(observer =>
            {
                _enemiesConfigurations[enemyId]
                    .Do(observer.OnNext)
                    .DoWhenAbsent(() => observer.OnError(new Exception("There is no Enemy config for that Enemy ID")));
                
                observer.OnCompleted();

                return Disposable.Empty;
            });
        }

        private Dictionary<string, Maybe<EnemyConfigDto>> GetMockEnemiesConfig()
        {
            var enemyOneConfig = new EnemyConfigDto(20, 2.5f);
            var enemyTwoConfig = new EnemyConfigDto(40, 1.5f);

            var config = new Dictionary<string, Maybe<EnemyConfigDto>>();
            config.Add("enemy1", enemyOneConfig.ToMaybe());
            config.Add("enemy2", enemyTwoConfig.ToMaybe());
            
            return config;
        }
    }
}