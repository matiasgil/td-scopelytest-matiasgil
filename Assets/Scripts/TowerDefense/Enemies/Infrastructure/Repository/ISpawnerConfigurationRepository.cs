﻿using System;
using TowerDefense.Enemies.Core.Domain.DTO;

namespace TowerDefense.Enemies.Infrastructure.Repository
{
    public interface ISpawnerConfigurationRepository
    {
        public IObservable<SpawnerConfigurationDto> GetSpawnerConfigFor(string mapId);
    }
}