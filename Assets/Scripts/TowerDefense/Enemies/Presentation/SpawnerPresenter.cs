﻿using TowerDefense.Enemies.Core.Actions;
using TowerDefense.Enemies.Core.Domain;
using UniRx;
using Utils.Maybe;

namespace TowerDefense.Enemies.Presentation
{
    public class SpawnerPresenter
    {
        private readonly ISpawnerView _view;
        private readonly CreateSpawnerAction _createSpawnerAction;

        public SpawnerPresenter(ISpawnerView view,
            CreateSpawnerAction createSpawnerAction)
        {
            _view = view;
            _createSpawnerAction = createSpawnerAction;
            
            _createSpawnerAction.Execute()
                .Do(SubscribeSpawnerObservables)
                .Subscribe()
                .Dispose();
        }

        private void SubscribeSpawnerObservables(Spawner spawner)
        {
            var disposable = spawner.SpawnEnemies()
                .Do(x => x.Do(_view.SpawnEnemies))
                .Subscribe();
            
            _view.DisposeWhenDestroyed(disposable);
        }
    }
}