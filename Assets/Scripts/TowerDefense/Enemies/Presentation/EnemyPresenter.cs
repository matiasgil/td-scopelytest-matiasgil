﻿using TowerDefense.Enemies.Adapter;
using TowerDefense.Enemies.Core.Actions;
using TowerDefense.Enemies.Core.Domain;
using UniRx;
using Utils;

namespace TowerDefense.Enemies.Presentation
{
    public class EnemyPresenter
    {
        private readonly IEnemyView _view;
        private readonly IEnemiesMapAdapter _enemiesMapAdapter;

        public EnemyPresenter(IEnemyView view, 
            string enemyId,
            IEnemiesMapAdapter enemiesMapAdapter,
            CreateEnemyAction createEnemyAction)
        {
            _view = view;
            _enemiesMapAdapter = enemiesMapAdapter;

            enemiesMapAdapter.GetMapSpawnPoint()
                .Do(MoveToSpawnLocation)
                .Subscribe()
                .Dispose();

            var disposable = createEnemyAction.Execute(enemyId)
                .Do(SetUpEnemy)
                .Subscribe();
            
            _view.DisposeWhenDestroyed(disposable);
        }

        private void SetUpEnemy(Enemy enemy)
        {
            _view.SetSpeed(enemy.Speed());
            
            _view.DamageReceived += enemy.ApplyDamage;

            var disposable = enemy.CurrentHealth()
                .Do(_view.UpdateHealth)
                .Subscribe();
            
            _view.DisposeWhenDestroyed(disposable);

            _enemiesMapAdapter.GetMapBasePoint()
                .Do(_view.SetDestination)
                .Subscribe()
                .Dispose();
        }

        private void MoveToSpawnLocation(PlanePosition position)
        {
            _view.MoveToSpawnPoint(position);
        }
    }
}