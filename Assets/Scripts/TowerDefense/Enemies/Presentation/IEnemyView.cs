﻿using System;
using Utils;

namespace TowerDefense.Enemies.Presentation
{
    public interface IEnemyView
    {
        event Action<int> DamageReceived;
        void DisposeWhenDestroyed(IDisposable disposable);
        void MoveToSpawnPoint(PlanePosition position);
        void UpdateHealth(int currentHealth);
        void SetSpeed(float speed);
        void SetDestination(PlanePosition planePosition);
    }
}