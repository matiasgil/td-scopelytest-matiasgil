﻿using System;

namespace TowerDefense.Enemies.Presentation
{
    public interface ISpawnerView
    {
        void SpawnEnemies(string[] enemiesId);
        void DisposeWhenDestroyed(IDisposable disposable);
    }
}