﻿namespace TowerDefense.Enemies.Adapter
{
    public interface IEnemiesGameLevelAdapter
    {
        void Won();
    }
}