﻿using System;

namespace TowerDefense.Enemies.Adapter
{
    public interface IEnemiesGameClockAdapter
    {
        public IObservable<long> GameClockOneSecondObservable();
    }
}