﻿using System;
using Utils;

namespace TowerDefense.Enemies.Adapter
{
    public interface IEnemiesMapAdapter
    {
        public IObservable<string> GetSelectedMap();

        IObservable<PlanePosition> GetMapSpawnPoint();

        IObservable<PlanePosition> GetMapBasePoint();
    }
}