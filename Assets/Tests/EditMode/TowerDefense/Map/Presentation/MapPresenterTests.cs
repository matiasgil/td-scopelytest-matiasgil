using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine.TestTools;
using NSubstitute;
using TowerDefense.Map.Core.Actions;
using TowerDefense.Map.Core.Domain;
using TowerDefense.Map.Presentation;
using UniRx;
using Utils;

namespace Tests.TowerDefense.Map.Presentation
{
    public class MapPresenterTests
    {

        private MapPresenter _mapPresenter;
        private CreateMapAction _createMapAction;
        private IMapView _mapView;

        private const string MAP_ID = "map_123123";
        private const int MAP_WIDTH = 111;
        private const int MAP_HEIGHT = 2;


        [SetUp]
        public void MapPresenterSetUp()
        {
            _createMapAction = Substitute.For<CreateMapAction>();
            _mapView = Substitute.For<IMapView>();

            _mapPresenter = new MapPresenter(_mapView, _createMapAction);
        }

        [Test]
        public void PresentsMapWithCorrectSizes()
        {
            GivenAGameMapCreated();

            WhenViewIsInitialized();

            ThenViewShowsMapWithCorrectSizes();
        }

        private void GivenAGameMapCreated()
        {
            var gameMap = new GameMap(MAP_ID,
                MAP_WIDTH, 
                MAP_HEIGHT, 
                new Dictionary<string, PlanePosition>(),
                new PlanePosition(0,0));
            
            var observable = Observable.Create<GameMap>(observer =>
                {
                    observer.OnNext(gameMap);
                    
                    observer.OnCompleted();
                    
                    return Disposable.Empty;      
                }
            );
            
            _createMapAction.Execute().Returns(observable);
        }

        private void WhenViewIsInitialized()
        {
            _mapView.Init += Raise.Event<Action>();
        }

        private void ThenViewShowsMapWithCorrectSizes()
        {
            _mapView.Received().ShowMap(MAP_ID);
        }
    }
}
